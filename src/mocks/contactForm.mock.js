import inter from './languageInterface'

const lang = window.navigator.language || 'en'
const dictionary = (label) => {
  console.log(label)
  return inter.en[label]
}
export const formMock = [
  {
    name: 'email',
    label: dictionary('emailLabel'),
    placeholder: 'some@incodewetrust.ru',
    type: 'Input',
    position: 'md-12'
  },
  {
    name: 'phone',
    label: dictionary('phoneLabel'),
    placeholder: '+7 (777) 777-77-77',
    type: 'Input',
    position: 'md-12'
  },
  {
    name: 'fullname',
    label: dictionary('fullNameLabel'),
    placeholder: dictionary('namePlaceholder'),
    type: 'Input',
    position: 'md-12'
  },
  {
    name: 'story',
    label: dictionary('descriptionLabel'),
    placeholder: dictionary('descriptionPlaceholder'),
    type: 'Textarea',
    position: 'md-12'
  }
]
