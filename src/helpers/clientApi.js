/* eslint-disable */
import axios from 'axios'

const API = 'https://incodewetrust.dev'

const clientApi = async (method, url, body) => {
  const responseUrl = `${API}/${url}`
  try {
    const { data = {} } = await axios[method](responseUrl, body)
    return data
  } catch (err) {
    throw err
  }
}

export {
  clientApi
}
